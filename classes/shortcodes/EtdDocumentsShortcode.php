<?php

namespace Grav\Plugin\Shortcodes;

use Grav\Common\Page\Page;
use Grav\Plugin\EtdDocuments\EtdDocuments;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;


class EtdDocumentsShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('etd-documents', function(ShortcodeInterface $sc) {

            //On récupère le lien de la page parente
            if(isset($this->shortcode->getPage()->header()->link)){
                $link = $this->shortcode->getPage()->header()->link;
            } else {
                $link = $this->shortcode->getPage()->route();
            }

            /**
             * @var Page $page
             */
            $page = $this->grav['pages']->find($link, true);
	        $etdDocuments = new EtdDocuments();
            $docs = $etdDocuments->getDocs($page);

            usort($docs, function($a, $b) {
                return $a["date"] < $b["date"];
            });


            $output = $this->twig->processTemplate(
                'partials/etd-documents.html.twig',
                [
                    "documents" => $docs,
                    "page" => $page
                ]
            );

            return $output;
        });

        $this->shortcode->getHandlers()->add('etd-document', function(ShortcodeInterface $sc) {

            $url_exploded = explode('/',$sc->getParameter('file'));


            $name = end($url_exploded);

            //var_dump($this->grav['uri']->base() . $sc->getParameter('file'));die;
            $page = $this->grav['page'];
	        $etdDocuments = new EtdDocuments();
            $output = '';
            if ($page->find(dirname($sc->getParameter('file')))){
                $thumbnail = $etdDocuments->genPdfThumbnail($page->find(dirname($sc->getParameter('file')))->media()->path() . '/' . urldecode($name), urldecode($name));

                $output = $this->twig->processTemplate(
                    'partials/etd-document.html.twig',
                    [
                        "page" => $page,
                        "route" => dirname($sc->getParameter('file')),
                        "thumbnail" => $thumbnail,
                        "document" => $sc->getParameter('file'),
                        "title" => $sc->getParameter('title'),
                        "name" => $name
                    ]
                );
            }
            

            return $output;
        });

        $this->shortcode->getHandlers()->add('etd-link', function(ShortcodeInterface $sc) {

            $url = $sc->getParameter('url');
            $target = $sc->getParameter('target');

            return '<a href="' . $url . '" target="' . $target . '">' . $sc->getContent() . '</a>';

        });
    }
}
