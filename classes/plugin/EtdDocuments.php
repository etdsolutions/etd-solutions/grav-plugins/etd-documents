<?php
namespace Grav\Plugin\EtdDocuments;

use Grav\Common\Grav;
use Gregwar\Image\Adapter\Imagick;

class EtdDocuments
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
    }

    public function genPdfThumbnail($source, $target)
    {
        //$source = realpath($source);

        $target = str_replace(".pdf", "", $target). '.jpg';

        $destination = dirname($source).DIRECTORY_SEPARATOR.$target;

        if(!file_exists($destination) && file_exists($source)){

            $im     = new \Imagick($source."[0]"); // 0-first page, 1-second page
            $im->setImageBackgroundColor('#ffffff');

            //$im->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE);
           // $im = $im->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);
            $im->setimageformat("jpg");

            $im->thumbnailimage(420, 594, true, false); // width and height
            $result = $im->writeimage($destination);
            $im->clear();
            $im->destroy();
        }
        return $target;
    }

    public function getDocs($page) {
        $docs = [];

        if (isset($page->header()->sections)) {
            foreach ($page->header()->sections as $section) {

                if (isset($section['documents'])) {
                    foreach ($section['documents'] as $d) {
                        if (isset($d['document'])) {
                            foreach ($d['document'] as $item) {
                                $etddocuments = new EtdDocuments();
                                $etddocuments->genPdfThumbnail($page->path() . '/' . $item['path'], $item['name']);

                                $item['route'] = $page->route();
                                $item['thumbnail'] = str_replace(".pdf", "",$item['name']) . '.jpg';
                                $item['date'] = filemtime($page->path() . '/' . $item['path']);

                                $item['title'] = $d['title'];
                                $item['category'] = isset($section['title']) ? $page->title() . ' > ' . $section['title'] : $page->title();

                                $docs[] = $item;
                            }
                        }
                    }
                }
            }
        }

        foreach ($page->children()->routable()->published() as $child) {
            if (isset($child->header()->sections)) {
                foreach ($child->header()->sections as $section) {

                    if (isset($section['documents'])) {
                        foreach ($section['documents'] as $d) {
                            if (isset($d['document'])) {
                                foreach ($d['document'] as $item) {

                                    $etddocuments = new EtdDocuments();
                                    $etddocuments->genPdfThumbnail($child->path() . '/' . $item['path'] , $item['name']);

                                    $item['route'] = $child->route();
                                    $item['thumbnail'] = str_replace(".pdf", "",$item['name']) . '.jpg';
                                    $item['date'] = filemtime($child->path() . '/' . $item['path']);


                                    $item['title'] = $d['title'];
                                    $item['category'] = $section['title'] ? $page->title() . ' > ' . $section['title'] : $page->title();

                                    $docs[] = $item;
                                }
                            }
                        }
                    }
                }
            }

            $docs = array_merge($docs, self::getDocs($child));
        }

        return $docs;

    }
}
