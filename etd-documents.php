<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Plugin;
use Grav\Common\Utils;
use Grav\Plugin\EtdDocuments\EtdDocuments;

/**
 * Class GoogleMapsPlugin
 * @package Grav\Plugin
 */
class EtdDocumentsPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        // Don't proceed if we are in the admin plugin
        if (Utils::isAdminPlugin()) { // this could be replaced by self::isAdmin, if the isAdmin function were static
            return [];
        }

        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onShortcodeHandlers' => ['onShortcodeHandlers', 0],
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            'registerNextGenEditorPlugin' => ['registerNextGenEditorPluginShortcodes', 0],
            'registerEtdEditorPlugin' => ['registerNextGenEditorPluginShortcodes', 0],
        ];
    }

    /**
     * [onPluginsInitialized:100000] Composer autoload.
     *
     * @return ClassLoader
     */
    public function autoload()
    {
        return require __DIR__ . '/vendor/autoload.php';
    }

    public function onTwigTemplatePaths()
    {
        $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
    }

    public function onTwigSiteVariables()
    {
        $this->grav['twig']->twig_vars['etddocuments'] = new EtdDocuments();
    }

        /**
     * Initialize configuration
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;
            return;
        }

        class_alias(EtdDocuments::class, 'Grav\\Plugin\\EtdDocuments');

        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
        ]);
    }

    /**
     * Initialize configuration
     */
    public function onShortcodeHandlers()
    {
        $this->grav['shortcode']->registerAllShortcodes(__DIR__ . '/classes/shortcodes');
    }

    public function registerNextGenEditorPluginShortcodes($event) {
        $plugins = $event['plugins'];

        $plugins['js'][] = 'plugin://etd-documents/nextgen-editor/shortcodes/etd-documents.js';
        $plugins['js'][] = 'plugin://etd-documents/nextgen-editor/shortcodes/etd-document/etd-document.js';
        $plugins['js'][] = 'plugin://etd-documents/nextgen-editor/shortcodes/etd-link/etd-link.js';

        $event['plugins']  = $plugins;
        return $event;
    }


}

